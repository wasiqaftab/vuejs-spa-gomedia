<?php


namespace App\Helpers;
use Illuminate\Http\Request;
use App\Models\LogActivity as LogActivityModel;


class LogActivity
{


    /**
     * @param $subject
     */
    public static function addToLog($subject): void
    {
        $log = [];
        $log['subject'] = $subject;
        $log['url'] = (new \Illuminate\Http\Request)->fullUrl();
        $log['method'] = (new \Illuminate\Http\Request)->method();
        $log['ip'] = (new \Illuminate\Http\Request)->ip() ?? "127.0.0.1";
        $log['agent'] = (new \Illuminate\Http\Request)->header('user-agent');
        $log['user_id'] = auth()->check() ? auth()->user()->id : 1;
        LogActivityModel::create($log);
    }


    /**
     * @return mixed
     */
    public static function logActivityLists()
    {
        return LogActivityModel::latest()->get();
    }


}
