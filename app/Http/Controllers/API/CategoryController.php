<?php

namespace App\Http\Controllers\API;

use App\Helpers\LogActivity;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    /**
     * @return array
     */
    public function index(): array
    {
        $category = Category::all()->toArray();
        return array_reverse($category);
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request): \Illuminate\Http\JsonResponse
    {
        LogActivity::addToLog('Category - Added');
        $category = new Category([
            'name' => $request->name,
            'description' => $request->description
        ]);
        $category->save();

        return response()->json('The category successfully added');
    }



    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id): \Illuminate\Http\JsonResponse
    {
        LogActivity::addToLog('Category - Edited');
        $category = Category::find($id);
        return response()->json($category);
    }



    /**
     * @param         $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request): \Illuminate\Http\JsonResponse
    {
        LogActivity::addToLog('Category - Updated');
        $category = Category::find($id);
        $category->update($request->all());

        return response()->json('The category successfully updated');
    }


    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id): \Illuminate\Http\JsonResponse
    {
        LogActivity::addToLog('Category - Deleted');
        $category = Category::find($id);
        $category->delete();

        return response()->json('The category successfully deleted');
    }
}
