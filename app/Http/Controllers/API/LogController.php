<?php

namespace App\Http\Controllers\API;

use App\Helpers\LogActivity;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class LogController extends Controller
{

    /**
     * @param Request $request
     *
     * @return array
     */
    public function index(Request $request): array
    {
        $logs = \App\Models\LogActivity::where("user_id",$request->user()->id)->get()->toArray();
        return array_reverse($logs);
    }


}
