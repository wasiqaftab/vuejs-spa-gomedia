import {createWebHistory, createRouter} from "vue-router";

import Home from '../pages/Home';
import About from '../pages/About';
import Register from '../pages/Register';
import Login from '../pages/Login';
import Dashboard from '../pages/Dashboard';

import AddCategory from "../components/AddCategory";
import Category from "../components/Category";
import EditCategory from "../components/EditCategory";
import Logs from "../components/Logs";

export const routes = [
    {
        name: 'home',
        path: '/',
        component: Home
    },
    {
        name: 'about',
        path: '/about',
        component: About
    },
    {
        name: 'register',
        path: '/register',
        component: Register
    },
    {
        name: 'login',
        path: '/login',
        component: Login
    },
    {
        name: 'dashboard',
        path: '/dashboard',
        component: Dashboard
    },
    {
        name: 'category',
        path: '/categories',
        component: Category
    },
    {
        name: 'logs',
        path: '/logs',
        component: Logs
    },
    {
        name: 'addcategory',
        path: '/category/add',
        component: AddCategory
    },
    {
        name: 'editcategory',
        path: '/category/edit/:id',
        component: EditCategory
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes: routes,
});

export default router;
